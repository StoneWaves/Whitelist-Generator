package stonewaves.Whitelist;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.IOException;

public class gui extends Application {


    private String ignFile = "";
    public static volatile boolean complete = false;

    @Override
    public void start(final Stage primaryStage) throws InterruptedException {

        //Window setup
        primaryStage.setTitle("Client Updater");
        final GridPane pane = new GridPane();
        pane.setAlignment(Pos.CENTER);
        pane.setHgap(10);
        pane.setVgap(10);
        pane.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(pane, 700, 275);



        //--------------------------------------------------------------------------------------------------------------
        //Add the Copy display stuff
        final Text StatusInfo = new Text("");
        StatusInfo.setFont(Font.font("Arial", FontWeight.NORMAL, 14));

        //--------------------------------------------------------------------------------------------------------------
        //Text box for JSON and errors
        //final TextField textField = new TextField ();
        //textField.setPrefHeight(800);
        //textField.setPrefWidth(800);
        //textField.setMinHeight(200);
        final Label JSONOutput = new Label();
        JSONOutput.setWrapText(true);





        //--------------------------------------------------------------------------------------------------------------
        //Application title
        Text sceneTitle = new Text("Whitelist Generation App");
        sceneTitle.setFont(Font.font("Arial", FontWeight.NORMAL, 20));
        sceneTitle.setUnderline(true);
        //--------------------------------------------------------------------------------------------------------------
        //File Selection code
        final FileChooser fileChooser = new FileChooser();
        final Button openButton = new Button("Select a file...");

        openButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {

                        File file = fileChooser.showOpenDialog(primaryStage);
                        if (file != null) {
                            try {
                                whitelist.run(String.valueOf(file), JSONOutput);
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                });

        //--------------------------------------------------------------------------------------------------------------
        //Whitelist copy code
        final Button copy = new Button("copy whitelist JSON");

        copy.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(final ActionEvent e) {

                        String NullString = "You need to run the whitelist generation first! \n" +
                                "To do this hit the 'Select file...' button in the gui and choose your file";
                        String myString = JSONOutput.getText();


                        if (JSONOutput.getText().isEmpty()){
                            StringSelection stringSelection = new StringSelection (NullString);
                            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard ();
                            clpbrd.setContents (stringSelection, null);
                        }
                        else{
                            StringSelection stringSelection = new StringSelection (myString);
                            Clipboard clpbrd = Toolkit.getDefaultToolkit().getSystemClipboard ();
                            clpbrd.setContents (stringSelection, null);
                        }

                    }
                });



        //--------------------------------------------------------------------------------------------------------------
        //Add graphincal elements
        pane.add(openButton, 1, 1);
        pane.add(StatusInfo, 2, 1);
        pane.add(JSONOutput, 3,1);
        pane.add(sceneTitle, 0, 0, 2, 1);
        pane.add(copy, 3, 2);

        //--------------------------------------------------------------------------------------------------------------
        //Show everything
        primaryStage.setScene(scene);
        primaryStage.show();






    }




}
