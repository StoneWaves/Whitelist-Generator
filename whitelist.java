package stonewaves.Whitelist;

import javafx.scene.control.Label;

import java.awt.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class whitelist {

    private static Desktop desktop = Desktop.getDesktop();
    public static final String API_URL = "https://api.mojang.com/profiles/minecraft";

    public static void run( String ignFile, javafx.scene.control.Label textField) throws IOException
    {
        BufferedReader ign_in = new BufferedReader( new FileReader( ignFile ) );
        BufferedWriter whitelist = new BufferedWriter( new FileWriter( "whitelist.json" ) );
        String line_in = "";
        String json_file = "[\n";

        //TODO
        System.out.print( "Processing..." );
        textField.setText( "Processing..." );


        do
        {
            line_in = ign_in.readLine();

            if( line_in != null )
            {
                //System.out.println( line_in );

                String json_request = "[ \"" + line_in + "\" ]";

                String json_response = executePost( API_URL, json_request, textField );

                //System.out.println( json_response );

                int first_quote = json_response.indexOf( "\"" );
                int second_quote = json_response.indexOf( "\"" , first_quote + 1);
                int third_quote = json_response.indexOf( "\"", second_quote + 1 );
                int fourth_quote = json_response.indexOf( "\"", third_quote + 1 );

                String uuid = json_response.substring( third_quote + 1, fourth_quote );
                //System.out.println( "UUID: " + uuid );
                json_file += nameAndUUIDToJSON( line_in, uuid );
            }
        }while( line_in != null );

        json_file += "]";

        StringBuffer buffer = new StringBuffer( json_file );
        buffer.deleteCharAt( buffer.lastIndexOf( "," ) );
        json_file = buffer.toString();

        whitelist.write(json_file);
        whitelist.close();


        System.out.println( "done" );
        gui.complete = true;
        output(textField);


    }

    public static String nameAndUUIDToJSON( String name, String uuid )
    {
        String json = "  {\n";
        json += "   \"uuid\": \"" + dashifyUUID( uuid ) + "\",\n";
        json += "   \"name\": \"" + name + "\"\n";
        json += "  },\n";

        return json;
    }

    public static String dashifyUUID( String uuid )
    {
        StringBuffer buffer = new StringBuffer( uuid );
        buffer.insert( 20, "-" );
        buffer.insert( 16, "-" );
        buffer.insert( 12, "-" );
        buffer.insert( 8, "-" );

        return buffer.toString();
    }

    public static String executePost(String targetURL, String urlParameters, Label textField) {
        HttpURLConnection connection = null;
        try {
            //Create connection
            URL url = new URL(targetURL);
            connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type",
                    "application/json");

            connection.setRequestProperty("Content-Length",
                    Integer.toString(urlParameters.getBytes().length));
            connection.setRequestProperty("Content-Language", "en-US");

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            DataOutputStream wr = new DataOutputStream (
                    connection.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.close();

            //Get Response
            InputStream is = connection.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder(); // or StringBuffer if not Java 5+
            String line;
            while((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            return response.toString();
        } catch (Exception e) {
            //TODO
            e.printStackTrace();
            gui.complete = true;
            return null;
        } finally {
            if(connection != null) {
                connection.disconnect();
            }
        }
    }

    public static void output(Label textField) throws IOException {

        BufferedReader br = new BufferedReader(new FileReader("whitelist.json"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append('\r');

                sb.append(System.lineSeparator());
                line = br.readLine();

            }
            String everything = sb.toString();
            textField.setText(everything);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            br.close();
        }

    }


}
